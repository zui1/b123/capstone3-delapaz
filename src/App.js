import React, { useState, useEffect } from 'react';
import './App.css';
import { BrowserRouter as Router } from 'react-router-dom';
import { Route, Switch } from 'react-router-dom';
import { UserProvider } from './userContext';
import { Container } from 'react-bootstrap';
import AppNavBar from './components/AppNavBar';

//PAGES
import Home from './pages/Home';
import Products from './pages/Products';
import Register from './pages/Register';
import Login from './pages/Login';
import Logout from './pages/Logout';
import NotFound from './pages/NotFound';
import ViewProduct from './pages/ViewProduct';
import Cart from './pages/Cart';
import AddProduct from './pages/AddProduct';
import ViewAllMensItems from './pages/ViewAllMensItems';
import ViewAllWomensItems from './pages/ViewAllWomensItems';
import ViewAllGadgetsShop from './pages/ViewAllGadgetsShop';
import ViewAllBagItems from './pages/ViewAllBagItems';
import ViewAllApparelItems from './pages/ViewAllApparelItems';
import UserOrder from './pages/UserOrder';





export default function App() {

    const [cart, setCart] = useState([])


    const [user, setUser] = useState({

        id: null,
        isAdmin: null

    })

    useEffect(() => {

        fetch('https://arcane-wave-04725.herokuapp.com/users/getUserDetails', {

                headers: {

                    'Authorization': `Bearer ${localStorage.getItem('token')}`

                }

            })
            .then(res => res.json())
            .then(data => {

                setUser({

                    id: data._id,
                    isAdmin: data.isAdmin

                })

            })

    }, [])

    const unsetUser = () => {

        localStorage.clear()

    }

    return ( 
        <>
        <UserProvider value={{user,setUser, unsetUser, cart, setCart}}>  
          <Router>
            <AppNavBar/>
            <Container fluid>
              <Switch>
                <Route exact path="/" component={Home}/>
                <Route exact path="/products" component={Products}/>
                <Route exact path="/products/:productId" component={ViewProduct}/>
                <Route exact path="/register" component={Register}/>
                <Route exact path="/login" component={Login}/>
                <Route exact path="/logout" component={Logout}/>
                <Route exact path="/cart" component={Cart}/>
                <Route exact path="/addProduct" component={AddProduct}/>
                <Route exact path="/viewAllMensItem" component={ViewAllMensItems}/>
                <Route exact path="/viewAllWomensItem" component={ViewAllWomensItems}/>
                <Route exact path="/viewAllGadgetsShop" component={ViewAllGadgetsShop}/>
                <Route exact path="/viewAllBagItems" component={ViewAllBagItems}/>
                <Route exact path="/viewAllApparelItems" component={ViewAllApparelItems}/>
                <Route exact path="/userOrder" component={UserOrder}/>
                <Route component={NotFound}/>
              </Switch>
            </Container>
          </Router>
        </UserProvider> 
        </>
    )




}