import React, { useContext, useEffect } from 'react';
import UserContext from '../userContext';
import Home from './Home';
import Swal from 'sweetalert2';
import { Redirect } from 'react-router-dom';
import * as Unicons from '@iconscout/react-unicons';

export default function Logout() {

    const { setUser, unsetUser } = useContext(UserContext);

    unsetUser();

    useEffect(() => {

        setUser({

            id: null,
            isAdmin: null

        })

        {
            Swal.fire({

            	 icon: "success",
                title: "THANK YOU SHOPPER!",
                showClass: {
                    popup: 'animate__animated animate__fadeInDown'
                },
                hideClass: {
                    popup: 'animate__animated animate__fadeOutUp'
                },
                text: `See you soon!`

            })
        }

    }, [])



    return ( <
        >


        <Redirect to="/"/> <
        />
    )

}