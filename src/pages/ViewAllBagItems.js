import React, { useState, useEffect, useContext } from 'react';
import Product from '../components/Product';
import UserContext from '../userContext';
import { Table, Button, Container, Row, Col } from 'react-bootstrap';
import './Products.css';
import SideDash from './SideDash';
import { Redirect } from 'react-router-dom';

export default function ViewAllBagItems() {

    const { user } = useContext(UserContext)
    console.log(user)

    const [allBagsArray, setAllBagsArray] = useState([]);
   
    useEffect(() => {

        fetch("https://arcane-wave-04725.herokuapp.com/products/viewAllBagItems")
            .then(res => res.json())
            .then(data => {

                setAllBagsArray(data.map(product => {

                    return (

                        <Product key={product._id} productProp={product}/>

                    )

                }))

            })

    }, [])


    console.log(allBagsArray);

    return ( 
         user.isAdmin
        ?
        <Redirect to="/products"/>
        :
    	<>
		<Container fluid>
		<Row>
        <Col md={3} className="mt-5 carousel2">
		
			<h1 className="shop-title text-center">BAG COLLECTIONS</h1>
            <SideDash/>
        </Col>
         <Col md={9} className="mt-5">
			<Row>
			{allBagsArray}
			</Row>
        </Col>    
        </Row>    
		</Container>
		</>

    )



}