import React, { useState, useEffect, useContext } from 'react';
import { Form, Button, Container, Row, Col, Nav } from 'react-bootstrap';
import Swal from 'sweetalert2';
import UserContext from '../userContext';
import { Redirect, useHistory, Link } from 'react-router-dom';
import './Register.css';
import Aos from "aos";
import "aos/dist/aos.css";

export default function Register() {

	useEffect(() => {
		Aos.init({duration:2000});
	}, [])


    const {user} = useContext(UserContext);

    const history = useHistory();

    const [firstName, setFirstName] = useState("");
    const [lastName, setLastName] = useState("");
    const [mobileNo, setMobileNo] = useState("");
    const [email, setEmail] = useState("");
    const [password, setPassword] = useState("");
    const [confirmPassword, setConfirmPassword] = useState("");
    const [isActive, setIsActive] = useState(false);

    useEffect(() => {

        if ((firstName !== "" && lastName !== "" && mobileNo !== "" && email !== "" && password !== "" && confirmPassword !== "") && (password === confirmPassword) && (mobileNo.length === 11)) {

            setIsActive(true)

        } else {

            setIsActive(false)

        }

    }, [firstName, lastName, mobileNo, email, password, confirmPassword])

    function registerUser(e) {

        e.preventDefault();

        fetch('https://arcane-wave-04725.herokuapp.com/users/registration', {

                method: 'POST',
                headers: {
                    "Content-Type": "application/json"
                },
                body: JSON.stringify({
                    firstName: firstName,
                    lastName: lastName,
                    mobileNo: mobileNo,
                    email: email,
                    password: password
                })

            })
            .then(res => res.json())
            .then(data => {

                console.log(data);

                if (data.email) {

                    Swal.fire({

                        icon: "success",
                        title: "Registration Succesful",
                        showClass: {
                    popup: 'animate__animated animate__fadeInDown'
                },
                hideClass: {
                    popup: 'animate__animated animate__fadeOutUp'
                },
                        text: `Thank you for registering, ${data.email}`

                    })

                    history.push('/login')

                } else {

                    Swal.fire({

                        icon: "error",
                        title: "Registration Failed.",
                        text: data.message

                    })

                }

            })

    }

    return (
		
		user.id
		?
		<Redirect to="/products"/>
		:
		<>
		<Container className="register">

		<Row className="row1">
		<Col md={5} className="d-none d-md-block div1" data-aos="slide-left">
		    <div className="r1">
			<h1 className="mt-1 text-center title1">WELCOME BACK!</h1>

			<p className="subinf text-center">To Keep connected with us please log in with your personal info</p>

			<Nav.Link className="button-1 text-center" as={Link} to="/login">SIGN IN</Nav.Link>
			</div>
		</Col>
		
		<Col md={7} data-aos="slide-right">
		<Form onSubmit={e => registerUser(e)}>
			<h1 className="my-5 text-center create">CREATE ACCOUNT</h1>
			<Row className="input">
			<Col md={6}>
			<Form.Group>
				<Form.Control type="text" value={firstName} onChange={e => {setFirstName(e.target.value)}} placeholder="First Name" required/>
			</Form.Group>
			</Col>
			<Col md={6}>
			<Form.Group>
				<Form.Control  type="text" value={lastName} onChange={e => {setLastName(e.target.value)}} placeholder="Last Name" required/>
			</Form.Group>
			</Col>
			<Col md={12} className="mt-3">
			<Form.Group>		
				<Form.Control type="number" value={mobileNo} onChange={e => {setMobileNo(e.target.value)}} placeholder="Mobile Number" required/>
			</Form.Group>
			</Col>
			<Col md={12} className="mt-3">
			<Form.Group>
				<Form.Control type="email" value={email} onChange={e => {setEmail(e.target.value)}} placeholder="Email Address" required/>
			</Form.Group>
			</Col>
			<Col md={12} className="mt-3">
			<Form.Group>
				<Form.Control type="password" value={password} onChange={e => {setPassword(e.target.value)}} placeholder="Password" required/>
			</Form.Group>
			</Col>
			<Col md={12} className="mt-3">
			<Form.Group>
				<Form.Control type="password" value={confirmPassword} onChange={e => {setConfirmPassword(e.target.value)}} placeholder="Confirm Password" required/>
			</Form.Group>
			</Col>
			<Col className="mt-3 text-center" >
			{

				isActive
				
				? <button className="btn-2"  type="submit">SIGN UP</button>
				: <button  className="btn-2" disabled>SIGN UP</button>

			}
			</Col>
			</Row>			
		</Form>
		</Col>
		</Row>
		</Container>
		</>

		)


}