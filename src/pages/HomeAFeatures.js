import React, {useEffect} from "react";
import {Container, Row, Col, Nav} from 'react-bootstrap';
import mens from '../images/men.jpg';
import women from '../images/women.jpg';
import './HomeAFeatures.css';
import {Link} from 'react-router-dom';
import Aos from "aos";
import "aos/dist/aos.css";

export default function HomeAFeatures() {

	useEffect(() => {
		Aos.init({duration:1400});
	}, [])

	return (



		<Container fluid className="px-0 apparelSec">
		
			<Row>
				<Col md={6} data-aos="slide-right" className="px-0" >
					<img className="pl-0" className="photo" id="men" src={mens} />
					<Nav.Link as={Link} to="/viewAllMensItem" className="content">MEN</Nav.Link>
				</Col>
				<Col md={6} data-aos="slide-left" className="px-0" >
					<img className="pr-0" className="photo" id="women" src={women} />
					<Nav.Link as={Link} to="/viewAllWomensItem" className="content">WOMEN</Nav.Link>
				</Col>	
			</Row>
		</Container>

		)

}