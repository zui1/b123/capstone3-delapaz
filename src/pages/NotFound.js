import React from 'react';
import Swal from 'sweetalert2';
import { Redirect } from 'react-router-dom';


export default function NotFound() {


    Swal.fire({

                 icon: "error",
                title: "SOMETHING WENT WRONG!",
               showClass: {
                    popup: 'animate__animated animate__shakeY'
                },
                text: `Redirected to Home Page`

            })

    return (



        <>	
    	 <Redirect to="/"/>
    	</>
    )

}