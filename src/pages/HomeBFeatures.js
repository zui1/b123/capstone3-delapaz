import React,{useEffect} from "react";
import { Container, Carousel, Card, Col, Row, Form } from 'react-bootstrap';
import bag1 from '../images/bag1.png';
import bag2 from '../images/bag2.png';
import bag3 from '../images/bag3.png';
import './HomeBFeatures.css';
import * as Unicons from '@iconscout/react-unicons';
import Aos from "aos";
import "aos/dist/aos.css";

export default function Home() {

  useEffect(() => {
    Aos.init({duration:1400});
  }, [])

    return (
    	<Container fluid className="bagSec bagss" data-aos="zoom-out-up">
      <Row>
      <Col lg={12} id="c1st">
        <Carousel fade id="carousel1">
 	        <Carousel.Item>
            <img
              className="d-block bags"
              src={bag1}
              alt="First slide"
              width="100%"
              height="600"
            />
          </Carousel.Item>
          <Carousel.Item>
            <img
              className="d-block bags"
              src={bag3}
              alt="Second slide"
              width="100%"
              height="600"
            />
          </Carousel.Item>
          <Carousel.Item>
            <img
              className="d-block bags"
              src={bag2}
              alt="Second slide"
              width="100%"
              height="600"
            />
          </Carousel.Item>
      </Carousel>
      </Col>
      <Col lg={12} id="email2">
      <Card id="subscription-email">
      <Row>
      <Col md={6} className="content01">
        <Card.Title className="subtitle1"><Unicons.UilEnvelopeAlt size="50" />   S T A Y&nbsp;&nbsp;U P&nbsp;&nbsp;T O&nbsp;&nbsp;D A T E</Card.Title>         
      </Col>  
      <Col md={4} className="content02">

       <Form.Group>
        <Form.Control id="input-subs" type="email"  placeholder="" required/>
      </Form.Group>
              
      </Col>
      <Col md={2} className="content03">
         <button className="btn-subs">SUBSCRIBE</button>
      </Col> 
      </Row>   
      </Card>
      </Col>
      </Row>
    </Container>
    )

}