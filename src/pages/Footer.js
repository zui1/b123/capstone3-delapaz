import React from "react";
import { Container, Carousel, Card, Col, Row, Form } from 'react-bootstrap';
import './Footer.css';
import * as Unicons from '@iconscout/react-unicons';

export default function Footer() {

	return (
		<Container fluid className="footer">
		<Container className="footer2">
			<Col md={12}>
			<h3 className="text-center followUs">FOLLOW & MESSAGE US ON:</h3>
			<div className="text-center"><Unicons.UilInstagramAlt size="60" /> <Unicons.UilFacebookF size="60" /> <Unicons.UilFacebookMessenger size="60" /></div>
			</Col>
			<Col lg={12}>
		        <h3 className="text-center copyR">© Copyright 2021 LARA'S ONLINE SHOP</h3>          
		     </Col>
		</Container>
		</Container>	

		)

}
