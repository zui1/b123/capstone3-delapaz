import React, { useState, useEffect, useContext } from 'react';
import { Form, Button, Container, Col, Row } from 'react-bootstrap';
import Swal from 'sweetalert2';
import { Redirect, useHistory } from 'react-router-dom'
import UserContext from '../userContext';
import './AddProduct.css';

export default function AddProduct() {

    const { user } = useContext(UserContext);

    const [name, setName] = useState("");
    const [image, setImage] = useState("");
    const [description, setDescription] = useState("");
    const [category, setCategory] = useState("");
    const [stocks, setStocks] = useState("");
    const [price, setPrice] = useState(0);
    const [isActive, setIsActive] = useState(true);

    useEffect(() => {

        if (name !== "" && image !== "" && description !== "" && category !== "" && stocks !== "" && price !== 0) {

            setIsActive(true)

        } else {

            setIsActive(false)

        }

    }, [name, image, description, category, stocks, price]);


    function createProduct(e) {

        e.preventDefault()

        let token = localStorage.getItem('token')

        fetch('https://arcane-wave-04725.herokuapp.com/products/', {

                method: 'POST',
                headers: {

                    'Content-Type': 'application/json',

                    'Authorization': `Bearer ${localStorage.getItem('token')}`

                },
                body: JSON.stringify({

                    name: name,
                    image: image,
                    description: description,
                    category: category,
                    stocks: stocks,
                    price: price

                })
            })
            .then(res => res.json())
            .then(data => {

                if (data.message) {
                    Swal.fire({

                        icon: "success",
                        title: "Prodcut Added.",
                        text: `Product has been added.`

                    })

                } else {
                    console.log(data)
                    Swal.fire({

                        icon: "success",
                        title: "Prodcut Added.",
                        text: `Product has been added.`

                    })
                   
                }

            })


        setName("");
        setImage("");
        setDescription("");
        setCategory("");
        setStocks(0);
        setPrice(0);

    };



   
		
	return (
		user.isAdmin !== true
		?
		<Redirect to="/"/>
		:
		<>
		<Container className="addP">
		<Row>
			<Col md={12}>
			<h1 className="shop-title text-center">ADD PRODUCT</h1>
			<Form onSubmit={ e =>  createProduct(e)}>
				<Form.Group controlId="name">
					<Form.Control 
						type="text"
						placeholder="Product Name"
						value={name}
						onChange={(e) => setName(e.target.value)}
						required
						className="mt-3"
					/>
				</Form.Group>
				<Form.Group controlId="image">
					<Form.Control 
						type="text"
						placeholder="Product Image URL"
						value={image}
						onChange={(e) => setImage(e.target.value)}
						required
						className="mt-3"
					/>
				</Form.Group>
				<Form.Group controlId="description">
					<Form.Control 
						type="text"
						placeholder="Product Description"
						value={description}
						onChange={(e) => setDescription(e.target.value)}
						required
						className="mt-3"
					/>
				</Form.Group>
				<Form.Group controlId="category">
					<Form.Control 
						type="text"
						placeholder="Product Category"
						value={category}
						onChange={(e) => setCategory(e.target.value)}
						required
						className="mt-3"
					/>
				</Form.Group>
				<Form.Group controlId="stocks">
					<Form.Control 
						type="number"
						placeholder="Stocks"
						value={stocks}
						onChange={(e) => setStocks(e.target.value)}
						required
						className="mt-3"
					/>
				</Form.Group>
				<Form.Group controlId="price">
					<Form.Control 
						type="number"
						placeholder="Price"
						value={price}
						onChange={(e) => setPrice(e.target.value)}
						required
						className="mt-3"
					/>
				</Form.Group>
				{
					isActive 
					? <button className="addbtn mt-3" type="submit" >Submit</button>
					: <button className="addbtn mt-3" type="submit"  disabled>Submit</button>
				}
			</Form>
			</Col>
			</Row>
		</Container>
		</>

		)
}



