import React from 'react';
import { Table, Button, Container, Row, Col, Form, Nav, Carousel } from 'react-bootstrap';
import * as Unicons from '@iconscout/react-unicons';
import ads1 from '../images/ads1.jpg';
import ads2 from '../images/ads2.jpg';

export default function SideDash() {

	return (

		<Carousel fade id="carousel2">
 	        <Carousel.Item>
            <img
              className="d-block ads"
              src={ads1}
              alt="First slide"
              width="100%"
              height="650"
             
            />
          </Carousel.Item>
          <Carousel.Item>
            <img
              className="d-block ads"
              src={ads2}
              alt="Second slide"
              width="100%"
              height="650"
             
            />
          </Carousel.Item>
      </Carousel>

		)

}